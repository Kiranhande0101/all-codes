#     1
#    121
#   12321
#  1234321
# 123454321
rows=int(input("Enter the number of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    n=1
    for z in range(x*2+1):
        if z<x:
            print(n,end=" ")
            n=n+1
        else:
            print(n,end=" ") 
            n=n-1
    print()        