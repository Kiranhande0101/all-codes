#    A
#   ABA
#  ABCBA
# ABCDCBA
rows=int(input("Enter the number of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    c=65
    for y in range(x*2+1):
        if y<x:
            print(chr(c),end=" ")
            c=c+1
        else:
            print(chr(c),end=" ")
            c=c-1
    print()          