# 2  5  10  17
#   26  37  50
#       65  82
#           101
 
rows=int(input("Enter the number of rows: "))
n=1
for x in range(rows):
    for y in range(x):
        print(" ",end=" ")
    for y in range(rows-x):
        print((n*n)+1,end=" ")
        n=n+1
    print()