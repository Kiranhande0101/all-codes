#            0
#        1   1
#    2   3   5
# 8  13  21  34
rows=int(input("Enter the number of rows: "))
num1=0
num2=1
sum=0
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    for y in range(x+1):
        print(num1,end=" ")
        sum=num1+num2
        num1=num2
        num2=sum
    print()    
