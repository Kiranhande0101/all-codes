# *---*

# --*--

# *---*

rows=int(input("Enter the number of rows: "))
for x in range(rows):
    for y in range(rows):
        if x%2==0:
          if (x==y) or (x+y==rows-1):
            print("*",end=" ")
          else:
              print(" ",end=" ")   
    print()