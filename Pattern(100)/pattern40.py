# 1  
# 2  4  
# 3  6  9  
# 4  8  12  16  
# 5  10  15  20  25  
# 6  12  18  24  30  36  
# 7  14  21  28  35  42  49  

rows=int(input("Enter the number of rows: "))
for x in range(rows):
    n=1
    for y in range(x+1):
        print(n*(x+1),end=" ")
        n=n+1
    print()    