#    *
#   ***
#  *****
# *******
#  *****
#   ***
#    *
n=int(input("Enter the number of rows: "))

for x in range(n):
    for y in range(n-x-1):
        print(" ",end=" ")
    for z in range(x*2+1):
        print("*",end=" ")
    print()
for i in range(n-1):
   for j in range(i+1):
      # print spaces
      print(" ", end=" ")
   for j in range(2*(n-i-1)-1):
      # print stars
      print("*", end=" ")
   print()
          