# A b C
# d E f
# G h I

rows=int(input("Enter the no of rows: "))
cols=int(input("Enter the no of columns: "))
c=65
n=97
for x in range(rows):
   for y in range(cols):
      if (x+y)%2==0:
          print(chr(c),end=" ")
      else:
         print(chr(n),end=" ")
      n=n+1   
      c=c+1
   print()    