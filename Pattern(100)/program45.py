#          a
#       2  1
#    c  b  a
# 4  3  2  1
rows=int(input("Enter the no of rows: "))

for x in range(rows):
    n=1+x
    c=97+x
    for y in range(rows-x-1):
        print(" ",end=" ")
    for y in range(x+1):
        if x%2!=0:
            print(n,end=" ")
            n=n-1   
        else:
            print(chr(c),end=" ")
            c=c-1
            
    print()             