#    *
#   ***
#  *****
# *******
#  *****
#   ***
#    *
rows = int(input("Enter the number of rows: "))
n=(rows//2)+1
for x in range(n):
    for y in range((n)-x-1):
        print(" ", end=" ")
    for z in range((x*2)+1):
        print("*", end=" ")
    print()
for x in range((n)-1):
    for y in range(x+1):
        print(" ", end=" ")
    for z in range(2*(n-x-1)-1):
        print("*", end=" ")
    print()
