#             9
#         18  27
#     36  45  54
# 63  72  81  90
rows=int(input("Enter the no. of rows: "))
n=1
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end="\t")
    for y in range(x+1):
        print(n*9,end="\t")
        n=n+1
    print()