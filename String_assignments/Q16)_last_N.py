# Write a program which accepts strings from the user and
# accept number N then copy the last N character into some other
# string.
# Input: India is my
# N : 5
# Output: is my
str1=input("Enter the string: ")
n=int(input("Enter the last number of char that you want to copy in another string: "))
str2=""
length=len(str1)
for x in range(length-n,length):
    str2=str2+str1[x]
print(str2)    
