# Write a program which accepts two strings from the user
# and compares two strings. If both strings are equal then return 0
# otherwise return difference between first mismatch character
# (Implement strcmp()).
# Input: FirStr FirStr
# Output: Both strings are equal.

str1=input("Enter the first string: ")
str2=input("Enter the second string: ")
if str1==str2:
    print("Both string are equal because difference is: ",abs(ord(str1[0])-ord(str2[0])))
else:
    print("Both string are not equal because difference is: ",abs(ord(str1[0])-ord(str2[0])) )