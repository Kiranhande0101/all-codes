# Write a program which accepts string from the user and
# then reverse the string till the last N characters without taking
# another string.
# Input: Hello World
# N : 5
# Output: Hello dlroW
def rstrrev(str1,n):
  size=len(str1)
  for x in range(size-n):
    print(str1[x],end="")
  for y in range(1,n+1):
    print(str1[-y],end="")     
str1=input("Enter the string: ")
n=int(input("Enter the number of char from last for reverse: ")) 
rstrrev(str1,n)