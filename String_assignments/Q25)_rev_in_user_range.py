# Write a program which accepts strings from the user and
# then accepts a range and reverse the string in that range without
# taking another string.
# Input: Hello World
# Start: 3
# End : 8
# Output: HeoW ollrld

str1=input("Enter the string: ")
start=int(input("Enter the start range: "))
end=int(input("Enter the end range: "))
last=(len(str1)-(end-1))
for x in range(start-1):
    print(str1[x],end="")
for y in range(end-1,start-2,-1):
    print(str1[y],end="")
for z in range(end,len(str1)) :      
    print(str1[z],end="")