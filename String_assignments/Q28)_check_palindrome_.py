# Write a program which accepts strings from the user
# and check whether the string is palindrome or not.
# Input: level
# Output: String is palindrome.
str1=input("Enter the string: ")
str2=""
for x in range(1,len(str1)+1):
    str2=str2+str1[-x]
if str1==str2:
    print(str1,":is palindrome string")   
else:
    print(str1,":is not palindrome string") 
