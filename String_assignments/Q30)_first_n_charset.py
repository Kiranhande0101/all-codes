# Write a program which sets first N characters in string
# to a specific character (Implement strnset()).
# Input: HelloWorld a
# N : 8
# Output: aaaaaaaald
str1=input("Enter the string: ")
char=input("Enter character that you want to make charset: ")
n=int(input("Enter the number of char that you want to make charset: "))
for x in range(len(str1)):
    if x<n:
        print(char,end="")
    else:
        print(str1[x],end="")
print()        
   