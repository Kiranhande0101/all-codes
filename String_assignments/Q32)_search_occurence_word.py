# Write a program which accepts string from the user and
# searches for the first occurrence of a specific character in string and
# returns the position at which character is found (Implement strchr()).
# Input: India is my country.
# Enter Char : m
# Output: Character m is found at position 9
str1=input("Enter the string: ")
char=input("Enter the character that you want to calculate occurence: ")
count=0
for x in range(len(str1)):
    if char==str1[x]:
        print(f"Character {char} is found at position {x}")  
print()             