'''sort first half in ascending order and second half in decending'''
def printorder(arr,n):
    arr.sort()

    i=0
    while i<n/2:
        print(arr[i])
        i=i+1
        j=n-1
        while j>=n/2:
            print(arr[j])
            j=j-1
arr=[1,90,34,89,7,9]
n=len(arr)
printorder(arr,n)

