''' write a program to swap two consecutive numbers in the array '''
import array
arr=array.array("i",[1,2,3,4,5,6,7,8])

for x in range(0,len(arr),2):
  temp=arr[x]
  arr[x]=arr[x+1]
  arr[x+1]=temp
  x=x+2
print(arr)