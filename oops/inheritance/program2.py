class company:
    
    cname="bajaj"
    workplace="chakan"
    
    def __init__(self):
        print("company constructor")
        self.teamcode="python code"
        
    @classmethod
    def facilities(cls):
        print("provide all functions......")
        print(cls.cname)
        print(cls.workplace)
        
class employee(company):
    role="developer"
    
    def __init__(self,empid,lang):
        print("child constructor")
        self.empid=empid
        self.lang=lang
        
    def info(self):
        print(self.empid)
        print(self.role)
        print(self.lang)
        print(self.workplace)
        
    @classmethod
    
    def skillset(cls):
        print(cls.role)
        
emp1=employee(25,"python developer")
emp2=employee(35,"java developer")

emp1.info()
emp2.info()
