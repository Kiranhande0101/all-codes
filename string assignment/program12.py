# Write a program which toggles the case of a string.
# Input: DevIce DriVer
# Output: dEViCE dRIvER
str1=input("enter the string:")
for x in str1:
    if x>="a" and x<="z":
        print(chr(ord(x)-32),end="")
    elif x>="A" and x<="z":
        print(chr(ord(x)+32),end="")
    else:
        print(x,end="")
print()