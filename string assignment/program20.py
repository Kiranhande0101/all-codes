#  Write a program which accepts two strings from the user
# and compares only first N characters of two strings. If both strings are # equal till first N characters then return 0 otherwise return difference
# between first mismatch character (Implement strncmp()).
# Input: FirStr FirNew
# N : 3
# Output: Both strings are equal.
str1=input("Enter the first string: ")
str2=input("Enter the second string: ") 
n=int(input("Enter the number of char that you want to compare from both string:"))
str1=str1[:n] 
str2=str2[:n]
if str1==str2:
    print("Both are equal string") 
else:
    print("Both are not equal string") 
