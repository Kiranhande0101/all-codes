# Write a program which accepts sentences from the user
# and print a number of small letters, capital letters and digits from that
# sentence.
# Input: abcDE 5Glm1 O
# Output: Small:5 Capital: 4 Digits: 2
str1=input("Enter the string: ")
small=0
capital=0
digit=0
for x in str1:
    if x>="a" and x<"z":
        small+=1
    elif x>="A" and x<="Z":
        capital+=1
    elif ord(x)>=48 and ord(x)<=57:
        digit+=1
print(f"Small:{small} capital:{capital} digit:{digit}") 